import java.io.RandomAccessFile;
import java.io.File;
import java.nio.channels.FileLock;
import java.util.Random;
/*
    Esta aplicación genera una serie de palabras aleatorias sin sentido real. Se utiliza con el proyecto "colaborar".
 */
public class Main {
    public static void main(String[] args) {

        RandomAccessFile raf = null;
        int n;
        File archivo = null;
        FileLock bloqueo = null;
        String nombreFichero = "";
        String palabras = "";

        // Se comprueban que se pasan argumentos.
        if (args.length > 0) {
            // Esto iguala los argumentos. n con el 1º argumento y nombreFichero con el 2º.
            n = Integer.parseInt(args[0]);
            nombreFichero = args[1];
        }else{
            // Valores por defecto si no se le pasan argumentos.
            nombreFichero = "miFicheroLenguaje.txt";
            n = 20;
        }

        try{
            // Aquí se generan todas las palabras.
            archivo = new File(nombreFichero);
            raf = new RandomAccessFile(archivo, "rwd");
            bloqueo = raf.getChannel().lock();
            StringBuffer buffer = null;

            String letras = new String("abcdefghijklmnopqrstuvwxyz");
            int tamano = letras.length();
            Random random = new Random();

            for(int i = 1; i<n+1; i++){
                int numero = (int)(Math.random() * 12 +1); // Con esto le damos un tamaño aleatorio a cada palabra.
                String palabra = "";

                for(int j = 1; j<= numero; j++){
                    palabra += letras.charAt(random.nextInt(tamano));
                    palabras = palabra.replaceAll("\\s","");
                }
                System.out.println(palabras);
                raf.seek(raf.length()); // Colocamos el raf al final del archivo.
                buffer = new StringBuffer(palabras); // Se guardan las palabras en el buffer.
                buffer.setLength(numero); // Igualamos el tamaño del buffer al de la palabra.
                raf.writeChars(buffer.toString().replaceAll("\\s",""));
                // Escribimos las palabras en el buffer en raf, se quitan tas y espacios en blanco.
                raf.writeChars("\n"); // Se añade un salto de linea.
            }
            bloqueo.release(); //Quitamos el bloqueo del archivo.
            bloqueo=null; // Y lo igualamos a null de nuevo.
        }catch (Exception e){
            System.err.println(e.toString());
        }finally {
            try{
                if(null != raf) raf.close(); // Con este try se cierra el raf y se libera el bloqueo.
                if(null != bloqueo) bloqueo.release();
            }catch (Exception e2){
                System.err.println(e2.toString());
                System.exit(1); // Si hay error se cierra.
            }
        }
    }
}